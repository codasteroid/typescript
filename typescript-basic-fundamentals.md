# What is TypeScript?

TypeScript is an open-source programming language that builds onto JavaScript by adding some extra features including optional static types, making it more scalable and maintainable. TypeScript code is not executed directly in the browser or JavaScript runtimes, but rather is first complied into a plain JavaScript code, which is then executed in those environments.

## Why use TypeScript?

TypeScript is a statically typed programming language, meaning that the type of the variable is explicitly defined in your code and is checked in compile time, which can catch type-related errors before the code is run. In dynamically typed languages, you can assign any value to a variable, and the type of this variable is determined in the runtime based on the value that is assigned to it.

So while dynamically typed languages offer flexibility and ease of use, as you don't need to explicitly declare variable types in your code, it doesn't offer the same level of error checking and type safely that statically typed languages provided.

## TypeScript Setup

TypeScript requires Node.js to run, you can download and install node.js from the [official website](https://nodejs.org). Once you have node.js installed, you can install TypeScript by running the following command in your terminal or command prompt:

```bash
npm i -g typescript
```

This will install TypeScript globally in your machine. Check the installed version of TypeScript to make sure that TypeScript is installed correctly in your machine:

```bash
tsc -v
```

##

Create a new file with **.ts** extension, for example `app.ts`. This is where you will write your TypeScript code. In your `app.ts`, write some TypeScript code, for example:

```typescript
function sayHello(name: string) {
	console.log(`Hello ${name}`);
}

const myName = 'abc';
sayHello(myName);
```
This code defines a function that takes a string argument and logs a greeting message to the console. Then, calls that function with a string variable.

Next, we need to compile our TypeScript code to execute it in the browser. In your terminal or command prompt, navigate to the directory where your `app.ts` file is located, and run the following command:

```bash
tsc app.ts
```
This will compile your TypeScript code, and generate a new JavaScript file called `app.js` in the same directory. Now, you can run the generated JavaScript in your terminal or command prompt using Node.js (make sure you are in the directory where the `app.js` is located):

```bash
node app.js
```
This will run your JavaScrit code and outputs the result.

## Setup config file

To configure TypeScript and further customize it by adding additional options that fit your needs, the `tsc --init` command can be used to create a basic `tsconfig.json` file with default options:

```bash
tsc --init
```
This will generate a default `tsconfig.json` with some basic options set, you can edit these options to match your needs. For example, you can add the **outDir** option to specify the output directory for compiled JavaScript files and **rootDir** option to specify the output directory for TypeScrit source files.

```json
{
  "compilerOptions": {
    /* Modules */
    "module": "commonjs",                                /* Specify what module code is generated. */
    "rootDir": "./src",                                  /* Specify the root folder within your source files. */
    "outDir": "./dist",                                  /* Specify an output folder for all emitted files. */
    // "removeComments": true,                           /* Disable emitting comments. */
    // "noEmit": true,                                   /* Disable emitting files from a compilation. */
    "esModuleInterop": true,                             /* Emit additional JavaScript to ease support for 
    "forceConsistentCasingInFileNames": true,            /* Ensure that casing is correct in imports. */
    /* Type Checking */
    "strict": true,                                      /* Enable all strict type-checking options. */
  }
}
```
Next, you can create these directories with the same names you assigned to the **rootDir** and **outDir** options. You can now move the `app.ts` file to the **rootDir** directory and delete the **app.js** because our TypeScript complier will compile all TypeScript files existed in the **rootDir** directory and outputs them to the **outDir** directory as Javascript files. 

Make sure you saved the `tsconfig.json` file, and run the TypeScript compiler using `tsc` command:

```bash
tsc
```
By setting these options in the `tsconfig.json` file, you can control where the compiled JavaScript files are outputted and where the TypeScript compiler looks for source files, making it easier to organize your project structure and build process.

## Create an HTML page

To use our TypeScript in an HTML page, let's create a simple web page within the **outDir** directory and add in it the following markup:

```html
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>TypeScript Playground</title>
</head>
<body>
	<h1>Hello World!</h1>
	<script src="app.js"></script>
</body>
</html>
```
Here, we added a script tag to load the comipled JavaScript file generated from the TypeScrit file. You can now open your HTML file `index.html` in the browser, and you must see the output of your TypeScript code in the browser console.





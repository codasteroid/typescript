"use strict";
// basic example
function sayHello(name) {
    console.log(`Hello ${name}`);
}
const myName = 'abc';
sayHello(myName);
// Basic types
let age = 37; // union
let city = 'Casablanca';
let isChecked = true;
let hobby;
hobby = 'basketball';
let fruits = ['peach', 'apple', 'orange']; // array
let nums = [3, 7, 5];
let random = [3, 'orange', 7, 'beach', 'summer', 5, 'passion'];
let person; // tuple
person = ['red', 37, true];
let places; // tuple array
places = [
    ['casablanca', 'morocco'],
    ['san diego', 'united states'],
    ['madrid', 'spain']
];
// Object
/// First approach 
let user_1 = {
    id: 3,
    name: 'blue',
    isMember: true
};
const user_2 = {
    id: 5,
    name: 'yellow',
    isMember: false
};
// Type assertion
let cid = 1;
let customerId = cid; // first approach
// et customerId = <string>cid; // second approach
customerId = '1';
// Functions
function add(x, y) {
    let sum = x + y;
    return sum.toString();
}
add(3, 7);
function log(message) {
    console.log(message);
}
log('hello user!');

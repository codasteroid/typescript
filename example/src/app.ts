// basic example

function sayHello(name: string) {
	console.log(`Hello ${name}`);
}

const myName = 'abc';
sayHello(myName);

// Basic types

let age: number | string = 37; // union
let city: string = 'Casablanca';
let isChecked: boolean = true;

let hobby: any;

hobby = 'basketball';

let fruits: string[] = ['peach', 'apple', 'orange']; // array
let nums: number[] = [3, 7, 5];
let random: any[] = [3, 'orange', 7, 'beach', 'summer', 5, 'passion'];

let person: [string, number, boolean]; // tuple
person = ['red', 37, true];

let places: [string, string][]; // tuple array
places = [
	['casablanca', 'morocco'],
	['san diego', 'united states'],
	['madrid', 'spain']
]

// Object

/// First approach 

let user_1: {id: number, name: string, isMember: boolean} = {
	id: 3,
	name: 'blue',
	isMember: true
};

/// Second approach 

type User = {
	id: number,
	name: string,
	isMember: boolean
};

const user_2: User = {
	id: 5,
	name: 'yellow',
	isMember: false
};

// Type assertion

let cid: any = 1;
let customerId = cid as string; // first approach
// let customerId = <string>cid; // second approach
customerId = '1';

// Functions

function add(x: number, y: number): string {
	let sum = x + y;
	return sum.toString();
}

add(3, 7);

function log(message: string | number): void {
	console.log(message);
}

log('hello user!');